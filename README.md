# 0310 Débuter avec docker

![Image d'intro](img/image1.png)

## Objectifs

* Créer un fichier Dockerfile
* Créer une image Docker
* Démarrer un container


## Matériel

* Applications
    * Docker Desktop
    * Visual Studio Code
* Documents
    * Cheatsheet DockerCLI
    * Cheatsheet Dockerfile


## Installation

```bash
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
```

## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre `<visa>` est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom

# Compte Docker Hub

L’accès au dépôt Docker Hub étant limité à 100 “pulls requests” toutes les 6 heures, nous allons créer un compte afin d’éviter tout souci lors des prochains exercices.
* Rendez-vous sur la page [Docker Hub](https://hub.docker.com/) pour créer un compte
* Connectez-vous à votre compte avec la commande \
`docker login -u [user] -p [password]` 

![alt_text](img/image2.png)



# Hello docker

* Affichez la version installée de docker 
* À l’aide de la commande « docker run », démarrez l’image « hello-world » 
* Comme proposé par « hello-world », démarrez ensuite un terminal dans un container Ubuntu 
* Affichez le contenu du dossier racine (`root` ou `/`) 
* Déconnectez-vous du container : commande `exit` ou `ctrl + d`
* Affichez les containers en cours d’exécution 
* Affichez tous les containers 
* Affichez la liste des images installées en local 
* Supprimez toutes les images locales 
* Vérifiez que toutes les images soient bien supprimées en affichant la liste